
package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    
    @Autowired
    private PostRepository postRepository;
    
    @Autowired
    private UserRepository userRepository;
            
    @Autowired
    JwtToken jwtToken;
    
    // creating a post
    public void createPost(String stringToken, Post post){
        // findByUsername to retrieve the user
        // Criteria for finding the user is from the jwtToken method(getUsernameFromToken)
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        //Title and content will come from the reqBody which is pass through the "post" identifier
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        
        // author from the token
        newPost.setUser(author);
        
        postRepository.save(newPost);

    }
    
    // getting all posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
    
    // Edit a user post
    public ResponseEntity updatePost(Long id, String stringToken, Post post){
        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();
        
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        
        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            
            return new ResponseEntity<>("Post has been successfully updated", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Your are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
        
    }

    // Delete a user post

    public ResponseEntity deletePost(Long id, String stringToken){
        Post postForDeletion = postRepository.findById(id).get();

        String postAuthor = postForDeletion.getUser().getUsername();

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);

            return new ResponseEntity<>("Post has been successfully deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Your are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }
    
    // Get a specific user's posts
    public ResponseEntity getUserPosts(String stringToken){
        String username = jwtToken.getUsernameFromToken(stringToken);
        
        User user = userRepository.findByUsername(username);
        
        List<Post> posts = postRepository.findByUser(user);
        return new ResponseEntity<>(posts, HttpStatus.OK);

    }
    

    
    
}
