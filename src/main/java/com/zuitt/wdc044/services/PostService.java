
package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    
    // create a post
    void createPost(String stringToken, Post post);
    
    //getting all post
    Iterable<Post> getPosts();
    
    // Edit a user post
    ResponseEntity updatePost(Long id, String stringtoken, Post post);
    
    // Delete a user post
    ResponseEntity deletePost(Long id, String stringToken);
    
    ResponseEntity getUserPosts(String stringToken);
}
