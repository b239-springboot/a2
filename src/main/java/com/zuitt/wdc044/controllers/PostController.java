package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(
            @RequestHeader(value = "Authorization") String stringToken,
            @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Get all posts
    // s04 Activity
    // Add a PostController method to retrieve all the posts from the database.
    // This method should respond to a Get request at the /posts endpoint, and 
    // return a new response entity that calls the getPosts() method from
    // postService.java. No arguments will be needed for this method.
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(
            @RequestHeader(value = "Authorization") String stringToken) {

        Iterable<Post> posts = postService.getPosts();
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

    // Edit a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid,
            @RequestHeader(value = "Authorization") String stringToken,
            @RequestBody Post post) {

        return postService.updatePost(postid, stringToken, post);
    }

    // Delete a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid,
            @RequestHeader(value = "Authorization") String stringToken) {

        return postService.deletePost(postid, stringToken);
    }
    
    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserPosts(
            @RequestHeader(value = "Authorization") String stringToken){
        
        return postService.getUserPosts(stringToken);
    }

}
